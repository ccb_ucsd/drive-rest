from openpyxl import load_workbook

__version__ = '1.0.0'

class MSExcel:
    '''MSExcel class that parses a MSExcel sheet's content'''
    def __init__(self, relativePath):
        self._path = relativePath
        self._wb = load_workbook(self._path, read_only=True)

    # Always assume there is only one worksheet
    def getByColumn(self, Column = 'A', start = 1, end = 10):
        ws = self._wb.get_active_sheet()
        cell_range = ws[Column+str(start):Column+str(end)]
        result = []
        for row in cell_range:
            for cell in row:
                result.append(cell.value)
        return result