//
//  FileListController.swift
//  DriveOAuthSwiftDemo
//
//  Created by Antonio Qiu on 4/12/16.
//  Copyright © 2016 Anton Qiu. All rights reserved.
//

import UIKit
import OAuthSwift
import SwiftCSV

class FileListController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  var oauthswift: OAuth2Swift?
  var fileList = [File]()
  @IBOutlet weak var fileListTableView: UITableView!
  
  func loadFileList() {
    let params = [
      "q": "mimeType='application/vnd.google-apps.spreadsheet'"
    ]
    
     // get the http layer then a file list
    oauthswift?.client.get(
      "https://www.googleapis.com/drive/v3/files",
      parameters: params,
      headers: [:],
      success: { (data, response) in
        let resultJSON = try! NSJSONSerialization.JSONObjectWithData(data, options: []) as? NSDictionary
        // print("GET SUCCESS: \(resultJSON)")
        
        guard let files = resultJSON!["files"] as? [NSDictionary]
          else {
            return
        }
        for file in files {
          self.fileList.append(File(json: file, client: self.oauthswift!))
        }
        // refresh
        self.fileListTableView.reloadData()
      }, failure: { (error) in
        print(error.localizedDescription)
    })
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
      
      // load file list
        loadFileList()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
      
    }
  
  override func viewDidAppear(animated: Bool) {
    // clear selection
    let selected = fileListTableView.indexPathForSelectedRow
    if (selected != nil) {
      fileListTableView.deselectRowAtIndexPath(selected!, animated: true)
    }
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return fileList.count
    }


    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
        // Configure the cell...
        cell.textLabel?.text = fileList[indexPath.row].name
        return cell
    }
  
//  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//    loadSingleFileAsCSV(fileList[indexPath.row])
//  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let fileDetail = segue.destinationViewController as! FileDetailController
    if segue.identifier == "ShowFileDetail" {
      fileDetail.oauthswift = oauthswift
      fileDetail.file = fileList[fileListTableView.indexPathForCell(sender as! UITableViewCell)!.row]
    }
  }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
