//
//  ViewController.swift
//  DriveOAuthSwiftDemo
//
//  Created by Antonio Qiu on 4/10/16.
//  Copyright © 2016 Anton Qiu. All rights reserved.
//

import UIKit

import OAuthSwift
import KeychainAccess

class ViewController: UIViewController {
  
  @IBOutlet weak var GoogleDriveAuthButton: UIButton!
  @IBOutlet var GoogleAuthInfoLabels: [UILabel]!
  @IBOutlet var GoogleAuthControlButtons: [UIButton]!
  @IBOutlet weak var usernameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var expiresLabel: UILabel!
  @IBOutlet weak var logoutButton: UIButton!
  @IBOutlet weak var refreshTokenButton: UIButton!
  
  var oauthswift: OAuth2Swift!

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    // OAuthSwift object
    oauthswift = OAuth2Swift(
      consumerKey: "907555596285-p3hpcrmpgm8meiuvl290mpctisq9c19f.apps.googleusercontent.com",
      consumerSecret: "",     // OAuth2 for ios now doesn't require a secret
      authorizeUrl: "https://accounts.google.com/o/oauth2/auth",
      accessTokenUrl: "https://accounts.google.com/o/oauth2/token",
      responseType: "code")
    oauthswift.authorize_url_handler = SafariURLHandler(viewController: self)
    
    // check oauth history
    let keychain = Keychain(service: "com.antonq.DriveOAuthSwiftDemo")
    let oauthToken = keychain["the_token_key"]  //optionals
    let oauthSecretToken = keychain["the_secret_token_key"] //optionals
    let oauthRefreshToken = keychain["refresh_token"]
    let oauthExpiresAt = keychain["expires_at"]
    if oauthToken != nil && oauthSecretToken != nil && oauthRefreshToken != nil {
      // retrieve token
      oauthswift.client.credential.oauth_token = oauthToken!
      oauthswift.client.credential.oauth_token_secret = oauthSecretToken!
      oauthswift.client.credential.oauth_refresh_token = oauthRefreshToken!
      let formatter = NSDateFormatter.init()
      formatter.timeStyle = NSDateFormatterStyle.FullStyle
      formatter.dateStyle = NSDateFormatterStyle.FullStyle
      oauthswift.client.credential.oauth_token_expires_at = formatter.dateFromString(oauthExpiresAt!)!
      
      // check expiration
      // not expired
      if !oauthswift.client.credential.isTokenExpired() {
        
        // hide auth button & show labels
        AuthElementsToggle(isLoggedIn: true)
      }
        // refresh token
      else {
        //TODO: REFRESH ? AUTO REFRESH?
        refreshToken()
      }
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func DriveAuth(sender: AnyObject) {
    Auth()
  }
  
  @IBAction func refreshTokenPressed(sender: AnyObject) {
    refreshToken()
  }
  
  @IBAction func logoutPressed(sender: AnyObject) {
    AuthElementsToggle(isLoggedIn: false)
  }
  
  /*
 OAuth procedure
 */
  func Auth() {
    
    //TODO: check expires
    
    // do auth
    oauthswift.authorizeWithCallbackURL(
      NSURL(string: "com.googleusercontent.apps.907555596285-p3hpcrmpgm8meiuvl290mpctisq9c19f:/oauth2Callback")!,
      scope: "email profile https://www.googleapis.com/auth/drive.readonly",       // 3 scope
      state: "readonly",
      success: { (credential, response, parameters) in
        print("SUCCESS: Authorized")
//        print("Token: " + credential.oauth_token)
//        print("Token_Secret" + credential.oauth_token_secret)
        print("Expires At:")
        print(credential.oauth_token_expires_at)
//        print(parameters)
        // var parameters =  [String: AnyObject]()
        // store credentials
        let keychain = Keychain(service: "com.antonq.DriveOAuthSwiftDemo")
        keychain["the_token_key"] = self.oauthswift.client.credential.oauth_token
        keychain["the_secret_token_key"] = self.oauthswift.client.credential.oauth_token_secret
        keychain["refresh_token"] = self.oauthswift.client.credential.oauth_refresh_token
        let formatter = NSDateFormatter.init()
        formatter.timeStyle = NSDateFormatterStyle.FullStyle
        formatter.dateStyle = NSDateFormatterStyle.FullStyle
        keychain["expires_at"] = formatter.stringFromDate(self.oauthswift.client.credential.oauth_token_expires_at!)
        self.AuthElementsToggle(isLoggedIn: true)
      },
      failure: {(error) -> Void in
        print("authorizeWithCallbackURL failed")
        print(error)
        //self.showAlert("Error", message: error.localizedDescription)
    })

  }
  
  func requestGETWithAction(RequestURI URI: String, successAction: (result: NSDictionary) -> Void) {
    let client = oauthswift.client
    var resultJSON: NSDictionary?
    
    client.get(
      URI,
      success: { (data, response) in
        //self.showAlert("Success", message: "Successful!")
        resultJSON = try! NSJSONSerialization.JSONObjectWithData(data, options: []) as? NSDictionary
        print("GET SUCCESS: \(resultJSON)")
        successAction(result: resultJSON!)
      },
      failure: { (error) in
        print(error.localizedDescription)
        if error.isExpireToken {
          self.Auth()
          self.requestGETWithAction(RequestURI: URI, successAction: successAction)
        }
      }
    )
  }
  
  func refreshToken() {
    //TODO: not working
    let URI = "https://www.googleapis.com/oauth2/v4/token"
    let refreshToken = oauthswift.client.credential.oauth_refresh_token
    let clientID = "907555596285-p3hpcrmpgm8meiuvl290mpctisq9c19f.apps.googleusercontent.com"
    // let redirectURI = "com.googleusercontent.apps.907555596285-p3hpcrmpgm8meiuvl290mpctisq9c19f:/oauth2Callback"
    
    let param =
    [
      "grant_type": "refresh_token",
      "refresh_token": refreshToken,
      "client_id": clientID,
      "client_secret": " "
    ]
    
    oauthswift.startAuthorizedRequest(
      URI,
      method: OAuthSwiftHTTPRequest.Method.POST,
      parameters: param,
      success: { (data, response) in
        let resultJSON = try! NSJSONSerialization.JSONObjectWithData(data, options: []) as? NSDictionary
        print("REFRESH SUCCESS: \(resultJSON!)")
        self.oauthswift.client.credential.oauth_token_secret = resultJSON!["access_token"] as! String
      }) { (error) in
        print(error.localizedDescription)
    }
  }
  
  func AuthElementsToggle(isLoggedIn loggedIn: Bool) {
    if loggedIn {
      requestGETWithAction(
        RequestURI: "https://www.googleapis.com/userinfo/v2/me",
        successAction: { (result) in
          self.usernameLabel.text = result["name"] as? String
          self.emailLabel.text = result["email"] as? String
          self.expiresLabel.text = self.oauthswift.client.credential.oauth_token_expires_at?.description
      })
      GoogleDriveAuthButton.hidden = true
      for thisLabel in GoogleAuthInfoLabels {
        thisLabel.hidden = false
      }
      for thisButton in GoogleAuthControlButtons {
        thisButton.hidden = false
      }
    } else {
      GoogleDriveAuthButton.hidden = false
      for thisLabel in GoogleAuthInfoLabels {
        thisLabel.hidden = true
      }
      for thisButton in GoogleAuthControlButtons {
        thisButton.hidden = true
      }
    }
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let navController = segue.destinationViewController as! UINavigationController
    let fileList: FileListController = navController.topViewController as! FileListController
    
    if segue.identifier == "FileList" {
      fileList.oauthswift = oauthswift
    }
  }
}

extension NSError {
  
  var isExpireToken: Bool {
    // reset error code for google drive: -2
    if self.code == -2 {
      if self.localizedDescription.rangeOfString("expired") != nil {
        return true
      }
      // or parse headers
//      if let reponseHeaders = self.userInfo["Response-Headers"] as? [String:String],
//        authenticateHeader = reponseHeaders["WWW-Authenticate"] ?? reponseHeaders["Www-Authenticate"] {
//        // you can split authenticateHeader to find "error"
//        let headerDictionary = authenticateHeader.headerDictionary
//        if let error = headerDictionary["error"] where error == "invalid_token" {
//          return true
//        }
//      }
    }
    return false
  }
}