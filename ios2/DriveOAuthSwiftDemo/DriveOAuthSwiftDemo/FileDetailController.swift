//
//  FileDetailController.swift
//  DriveOAuthSwiftDemo
//
//  Created by Antonio Qiu on 4/14/16.
//  Copyright © 2016 Anton Qiu. All rights reserved.
//

import UIKit
import OAuthSwift

class FileDetailController: UITableViewController {
  
  var oauthswift: OAuth2Swift?
  var file: File?
  var genes: [String]?

  @IBOutlet weak var geneListTableView: UITableView!

    override func viewDidLoad() {
      super.viewDidLoad()
      // parse and refresh
      file?.loadAsCSV(
        success: { (content) in
          self.genes = self.file?.parseByColumnWithHeader()
          self.geneListTableView.reloadData()
        }, onFailure: { (error) in
      })

      self.refreshControl = UIRefreshControl()
      self.refreshControl?.addTarget(self, action: #selector(FileDetailController.refreshFile(_:)), forControlEvents: UIControlEvents.ValueChanged)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)
    // Configure the cell...
    cell.textLabel?.text = genes![indexPath.row]
    return cell
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if genes == nil {
      return 0
    } else {
      return genes!.count
    }
  }
  
  // MARK: Refresh Table
  func refreshFile(sender: UIRefreshControl) {
    file?.loadAsCSV(
      success: { (content) in
        self.genes = self.file?.parseByColumnWithHeader()
        self.geneListTableView.reloadData()
        sender.endRefreshing()
      }, onFailure: { (error) in
    })
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
