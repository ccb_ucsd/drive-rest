//
//  File.swift
//  DriveOAuthSwiftDemo
//
//  Created by Antonio Qiu on 4/13/16.
//  Copyright © 2016 Anton Qiu. All rights reserved.
//

import Foundation
import OAuthSwift
import SwiftCSV

class File {
  var id: String!
  var kind: String!
  var mimeType: String!
  var name: String!
  var content: CSV?
  var genes: [String]?
  
  var oauthswift: OAuth2Swift?
  
  init(json: NSDictionary, client: OAuth2Swift) {
    self.id = json["id"] as! String
    self.kind = json["kind"] as! String
    self.mimeType = json["mimeType"] as! String
    self.name = json["name"] as! String
    self.oauthswift = client
  }
  
  func loadAsCSV(success onSuccess: (content: CSV)->Void, onFailure: (error: NSError)->Void) {
    let params = [
      "fileId": self.id,
      "mimeType": "text/csv"
    ]
    oauthswift?.client.get(
      "https://www.googleapis.com/drive/v3/files/fileId/export",
      parameters: params,
      headers: [:],
      success: { (data, response) in
        self.content = CSV(string: NSString(data: data, encoding: NSUTF8StringEncoding) as! String)
        onSuccess(content: self.content!)
        //TODO: can do better
      }, failure: { (error) in
        print(error.localizedDescription)
        onFailure(error: error)
    })
  }

  
  func parseByColumnWithHeader() -> [String] {
    // Header is ignored
    let genes = content!.columns.first?.1
    self.genes = genes
    return genes!
  }
}