#!/usr/bin/env bash

# A setup shell script that builds dependencies for Drive.server

# Assume Installed:
# 1. conda with a Python 2.7 environment, and with $PATH configured

# UPDATE
echo -n "updating from git repo ... "; git pull > /dev/null; echo "done."

# PACKAGES
echo -n "installing Google Api Client Library for Python ... "
pip install --upgrade google-api-python-client > /dev/null; echo "done."
echo -n "installing bottle ... "; pip install --upgrade bottle > /dev/null; echo "done."
echo -n "installing openpyxl ... "; conda install openpyxl > /dev/null; echo "done."
